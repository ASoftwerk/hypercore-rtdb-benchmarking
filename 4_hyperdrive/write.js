var hyperdrive = require('hyperdrive')
var drive = hyperdrive('./drive')
let Benchmark = require('benchmark');
let suite = new Benchmark.Suite;
let fs = require('fs');

console.log("Write test start in 4 sec...");

let data = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis molestias ipsum praesentium id? Modi atque iure vel error consectetur. Ducimus magnam at, iste et sint blanditiis maxime iure? Alias, delectus?';


function getDataInBytes(str) {
    console.log('Data' + ": " + str.length + " characters, " +
        Buffer.byteLength(str, 'utf8') + " bytes");
}


setTimeout(() => {
    getDataInBytes(data);
    suite.add("Write: ", function () {
        drive.writeFile('/text.txt', data, function (err) {
            if (err) throw err
        });
    }).on('cycle', function (event) {
        console.log(String(event.target));
    }).run({ 'async': true });
}, 4000);
// drive.readFile('./text.txt', 'utf-8', function (err, data) {
//     if (err) throw err
//     getDataInBytes(data)
// })

// function getFilesizeInBytes(filename) {
//     var stats = fs.statSync(filename);
//     var fileSizeInBytes = stats.size;
//     return fileSizeInBytes;
// }

// function getFilesizeInMegaBytes(filename) {
//     return (getFilesizeInBytes(filename) / (1024 * 1024) + ' Mb')
// }

// console.log(getFilesizeInMegaBytes('text_data.txt'));