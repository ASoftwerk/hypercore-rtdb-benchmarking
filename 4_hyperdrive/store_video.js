var hyperdrive = require('hyperdrive');
var drive = hyperdrive('./drive');
var base64 = require('file-base64');
let fs = require('fs-extra');

const fileName = 'film.mp4';

console.time('Write');
console.time('Read');
base64.encode(fileName, function (err, base64String) {
  console.log('File Size: ' + getFilesizeInMegaBytes(fileName))
  drive.writeFile('/video.mp4', base64String, function (err) {
    if (err) throw err
    console.timeEnd('Write');
    drive.readdir('/', function (err, list) {
      if (err) throw err
      // console.log(list)
      drive.readFile('/test.txt', 'utf-8', function (err, data) {
        if (err) throw err
        // console.log(data)
        console.timeEnd('Read');
      })
    })
  })
});


function getFilesizeInMegaBytes(filename) {
  var stats = fs.statSync(filename);
  var fileSizeInBytes = stats.size;
  return (Math.round(fileSizeInBytes / (1024 * 1024)) + ' MB');
}
