RTDB Benchmarks module 1 repository (here).
Alternative download it from:
https://mega.nz/file/jqhVEQqD#AsWH2de4frtrdY_7rzdTxS_LEm5336N9QvPg2h5AmLI

RTDB Benchmarks module 2 contains test data, so it is huge to be in this repo, please download it from:
https://mega.nz/file/vrw0xR4I#j2UTrsXp2mWwtiMgG8ycfC2FF2IJ7RomEdW_jbOEEeM


All results provided under Apache 2.0 license.
