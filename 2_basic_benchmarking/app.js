let hypercore = require('hypercore');
let feed = hypercore('./dataset', { valueEncoding: 'utf-8' });
let Benchmark = require('benchmark');
let suite = new Benchmark.Suite;
let fs = require('fs-extra');
var os = require('os-utils');
var osu = require('node-os-utils');
var cpu = osu.cpu;
var diskStat = require('disk-stat');

cpu.usage()
    .then(cpuPercentage => {
        console.log(`Usage before test: CPU (%): ${cpuPercentage}  MEM: ${Math.round(os.totalmem())} MB`)
    });
console.log("Test start in 5 sec...")
setTimeout(() => {
    var from = new Date().getTime();
    for (var i = 1; i <= 1; i++) {
        fs.readFile("names.txt", function (err, data) {
            if (err)
                console.log("read error: ", err);
            else {
                feed.append(data);
                fs.writeFile(__dirname + "/new_names.txt", data, function (err) {
                    if (err) {
                        console.log("write error: ", err);
                    } else {
                        console.log("Write Speed of Native fs module: " + (new Date().getTime() - from) / 1000, " ms");
                    }
                });
                diskStat.usageWrite({
                    units: 'KiB',
                },
                    function (mbPerSecond) {
                        console.log(`Write Speed: ${Math.round(mbPerSecond)} kb/s`);
                    });
            }
        });
    };
    cpu.usage()
        .then(cpuPercentage => {
            console.log(`Usage whithin test: CPU (%): ${cpuPercentage}  MEM: ${Math.round(os.totalmem())} MB`)
        }).then(() => {
            readDataFromLogs();
        })
}, 5000);





function readDataFromLogs() {
    console.log("Reading data test start in 2 sec...")
    setTimeout(() => {
        for (let index = 0; index < feed.length; index++) {
            feed.get(index, () => { });
        }
        diskStat.usageRead({
            units: 'KiB',
        },
            function (mbPerSecond) {
                console.log(`Read Speed: ${Math.round(mbPerSecond)} kb/s`);
            });
    }, 2000);
}


// ___________________ EXTRA CODE! _________________________
// function checkDiskSpeed(type) {
//     let unitsFormat = 'MiB'
//     if (type === 'read') {
//         diskStat.usageRead({
//             units: unitsFormat,
//         },
//             function (speed) {
//                 console.log(`Read Speed: ${speed} 'kb/s'`);
//             });
//     }
// }


// fs.readFile('names.txt', 'utf8', function (err, data) {
//     if (err) {
//         return console.log(err);
//     }
// feed.append(data, function (err) {
//     if (err) throw err
//     for (let index = 0; index < feed.length; index++) {
//         feed.get(index, console.log)
//     }
// });
// });





// suite.add("Append: ", function () {
//     feed.append('hello')
// }).on('cycle', function (event) {
//     console.log(String(event.target));
// }).run({ 'async': true }, { minSamples: 1 });

