var hypercore = require('hypercore');
var feed = hypercore('./logs', { valueEncoding: 'utf-8' });
var colors = require('colors/safe');

let randomText = Math.random().toString(36).substring(7);

feed.append(randomText, function (err) {
    if (err) throw err
    console.log(`Logs: ${feed.length}`);
    console.log(`Data: ${colors.green(feed.byteLength)} Bytes`);
    feed.get(feed.length - 1, console.log);
});