let hypercore = require('hypercore');
let feed = hypercore('./dataset4', { valueEncoding: 'utf-8' });
let Benchmark = require('benchmark');
let suite = new Benchmark.Suite;
let fs = require('fs-extra');
var os = require('os-utils');
var osu = require('node-os-utils');
var cpu = osu.cpu;
var diskStat = require('disk-stat');


suite.add("Append: ", function () {
    feed.append('testing')
}).on('cycle', function (event) {
    console.log(String(event.target));
}).run({ 'async': true }, { minSamples: 1 });
