let hypercore = require('hypercore');
let feed = hypercore('./dataset2', { valueEncoding: 'utf-8' });
let Benchmark = require('benchmark');
let suite = new Benchmark.Suite;
let fs = require('fs-extra');
var os = require('os-utils');
var osu = require('node-os-utils');
var cpu = osu.cpu;
var diskStat = require('disk-stat');

cpu.usage()
    .then(cpuPercentage => {
        console.log(`Usage before test: CPU (%): ${cpuPercentage}  MEM: ${Math.round(os.totalmem())} MB`)
    });
console.log("Test start in 5 sec...")
setTimeout(() => {
    var from = new Date().getTime();
    for (var i = 1; i <= 10; i++) {
        fs.readFile("text_data.txt", function (err, data) {
            if (err)
                console.log("read error: ", err);
            else {
                console.log("Write Speed of Native fs module: " + (new Date().getTime() - from) / 1000, " ms");
                diskStat.usageWrite({
                    units: 'KiB',
                },
                    function (mbPerSecond) {
                        console.log(`Write Speed: ${Math.round(mbPerSecond)} kb/s`);
                    });
                feed.append(data);
            }
        });
    };
    cpu.usage()
        .then(cpuPercentage => {
            console.log(`Usage whithin test: CPU (%): ${cpuPercentage}  MEM: ${Math.round(os.totalmem())} MB`)
        }).then(() => {
            readDataFromLogs();
        })
}, 5000);





function readDataFromLogs() {
    console.log("Reading data test start in 2 sec...")
    setTimeout(() => {
        for (let index = 0; index < feed.length; index++) {
            feed.get(index, () => { });
        }
        diskStat.usageRead({
            units: 'KiB',
        },
            function (mbPerSecond) {
                console.log(`Read Speed: ${Math.round(mbPerSecond)} kb/s`);
            });
    }, 2000);
}